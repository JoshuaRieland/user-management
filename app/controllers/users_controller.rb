class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def new
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    User.create( first_name: params[:first_name], last_name: params[:last_name], email: params[:email], password: params[:password])
    redirect_to '/users/index'
  end

  def update
    @user = User.find(params[:id])
    User.update(params[:id], :first_name => params[:first_name], :last_name => params[:last_name], :email => params[:email], :password => params[:password])
    redirect_to '/users/index'
  end

  def destroy
      @user = User.find(params[:id])
  end
  def delete
    User.delete(params[:id])
    redirect_to '/users/index'
  end

end
